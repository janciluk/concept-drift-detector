import pandas as pd

class Logger:
    """
    Logger submodule for storing the results of each round of drift detection.
    """
    def __init__(self):
        self.dates = []
        self.is_drifted = []
        self.drift_strength = []
        self.share_drifted_features = []
        self.drift_type = []
        self.f1 = []

    def log(self,date, drift_statistics, f1 = None):
        """Store the results from the current round of drift detection
        Args:
        date (Datetime/pd.Timestamp): 
            Time when the test was carried out
        drift_statistics (dictionary): 
            Statistics obtained from detector.get_drift_statistics() 
        f1 (float, optional): 
            F1 score of the model on data from when the test was carried out
        """   
        self.dates.append(date)
        self.is_drifted.append(drift_statistics["is_drifted"])
        self.drift_strength.append(drift_statistics["drift_strength"])
        self.share_drifted_features.append(drift_statistics["share_drifted_features"])
        if("drift_type" in drift_statistics):
            self.drift_type.append(drift_statistics["drift_type"])
        if(f1):
            self.f1.append(f1)

    def get_logs(self):
        """Get the stored detection results
        Returns:
            pd.Dataframe: Detection results for each logged date
        """   
        log_data = {
            "is_drifted": self.is_drifted,
            "drift_strength": self.drift_strength,
            "share_drifted_features": self.share_drifted_features
            }
        if(self.drift_type):
            log_data["Drift_type"] = self.drift_type 
        if(self.f1):
            log_data["f1"] = self.f1 
        return pd.DataFrame(data=log_data, index = self.dates)